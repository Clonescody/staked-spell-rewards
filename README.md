# Rewards calculator for sSPELL

This snippet has the main logic needed to calculate the cumulative USD and SPELL rewards earned from staking SPELL as sSPELL.

## Subgraph

> Generate a subgraph and add the modifications from here in your own repo, don't forget to codegen to be able to call the contracts ( `sSPELL.bind()` & `SPELL.bind()` ).

We'll use The Graph subgraphs to index the user's sSPELL balance and the sSPELL:SPELL ratio. For SPELL, we'll only need to track one transfer :

- SPELL from SWAPPER to sSPELL => SPELL distribution

We'll register a user balance on a sSPELL transfer to make sure we keep track of tokens sent away.

- sSPELL from NULL to USER => user staked
- sSPELL to NULL from USER => user unstaked

Because we keep track of the balance with transfers, we need an intermediate `UserBalanceTotal` entity to be used as a "variable" to carry the balance of the user through blocks.

## API/Script

> Types are defined in `./script/types.ts`

Once the subgraph is synced, you can query the pricePerShare history as a whole, and a user balance history with his address.

Once those are received, the typescript functions in `./script/script.ts` will allow the following :

- `getLlamaTokenPriceAtTimestamp` => get the price of a token at a specific timestamp from DefiLlama.
- `fillInMissingPpsGraphObjects` => takes in a pricePerShare history queried from the subgraph, and returns an array of pricePerShare organized by days. Usefull for a daily chart display.
- `getCumulativeRewardsForPps` => takes in a user balance history of sSPELL, and a daily organized pricePerShare history, to return a daily array of cumulative SPELL and USD equivalent rewards. More details in the function's comments.

Once plugged to an API that'll enable a daily chart of cumulative SPELL and USD rewards.
