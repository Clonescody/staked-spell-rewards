import {
  UserStakedSpellBalance,
  StakedSpellPricePerShare,
  RewardsDataPoint,
} from "./types";

// This functions takes in a user history and a pricePerShare change history
// Will return the cumulative USD and cumulative deposit token rewards timestamped
export const getCumulativeRewardsForPps = async (
  userBalanceHistory: UserStakedSpellBalance[],
  ppsChangeHistory: StakedSpellPricePerShare[],
  underlyingToken: string
): Promise<RewardsDataPoint[]> => {
  const firstDeposit = moment(
    parseInt(userBalanceHistory[0].timestamp, 10) * 1000
  );
  const daysSinceFirstDeposit = moment().diff(firstDeposit, "days");
  const startPpsEntity = ppsChangeHistory.filter(({ timestamp }) =>
    moment(parseInt(timestamp, 10) * 1000).isSameOrBefore(firstDeposit, "day")
  );

  let cumulativeAsset = 0.0;
  let cumulativeUSD = 0.0;
  const userRewardsGraphArray: RewardsDataPoint[] = [];
  // This follows the current state of the balance through the loop
  let currentUserBalance = parseFloat(userBalanceHistory[0].value);
  // This follows the current state of the ratio through the loop
  let currentRatio =
    startPpsEntity.length > 0
      ? parseFloat(startPpsEntity[startPpsEntity.length - 1].pricePerShare) // closest ratio before staking
      : 1.0;

  // For each day since first compounding
  for (let day = 0; day <= daysSinceFirstDeposit; day += 1) {
    const loopDay = moment().subtract(daysSinceFirstDeposit - day, "days");
    // Balances/pps history of the current loop day
    const balancesChangesOnThatDay = userBalanceHistory.filter(
      ({ timestamp }) =>
        moment(parseInt(timestamp, 10) * 1000).isSame(loopDay, "day")
    );
    const ppsChangeOnThatDay = ppsChangeHistory.filter(({ timestamp }) =>
      moment(parseInt(timestamp, 10) * 1000).isSame(loopDay, "day")
    );
    const didBalanceChangeOnThatDay = balancesChangesOnThatDay.length > 0;

    // Skip if no pps change
    if (ppsChangeOnThatDay.length === 0) {
      // Don't forget to update user balance
      if (didBalanceChangeOnThatDay) {
        currentUserBalance = parseFloat(
          balancesChangesOnThatDay[balancesChangesOnThatDay.length - 1].value
        );
      }
      userRewardsGraphArray.push({
        timestamp: loopDay.unix().toString(),
        asset: cumulativeAsset,
        usd: cumulativeUSD,
      });
      continue;
    }

    // From this point the price per share ratio changed during the day
    let assetRewardsOnThatDay = 0.0;
    let usdRewardsOnThatDay = 0.0;

    if (!didBalanceChangeOnThatDay) {
      // User balance didn't change during ratio change
      // simply calculate his rewards from the ratio
      await Promise.all(
        ppsChangeOnThatDay.map(
          async ({ timestamp: ppsChangeTimestamp, pricePerShare }) => {
            const assetPriceAtTimestamp = await getLlamaTokenPriceAtTimestamp(
              underlyingToken,
              parseInt(ppsChangeTimestamp, 10)
            );
            const ratioChange = parseFloat(pricePerShare) - currentRatio;
            assetRewardsOnThatDay += currentUserBalance * ratioChange;
            usdRewardsOnThatDay +=
              currentUserBalance * ratioChange * assetPriceAtTimestamp;
          }
        )
      );
    } else {
      // User balance changed the same day the ratio did
      // Needs to account if the ratio changed before or after a balance change
      // For each ratio change that day
      await Promise.all(
        ppsChangeOnThatDay.map(
          async ({ timestamp: ppsChangeTimestamp, pricePerShare }, _index) => {
            let balanceChangesBeforeDistribution: UserStakedSpellBalance[] = [];
            // Check for balances change on same day but before ratio change
            if (_index === 0) {
              balanceChangesBeforeDistribution =
                balancesChangesOnThatDay.filter(
                  ({ timestamp: dailyBalanceChangeTimestamp }) =>
                    parseInt(dailyBalanceChangeTimestamp, 10) <=
                    parseInt(ppsChangeTimestamp, 10)
                );
            } else {
              // Check for balances change on same day, before the ratio change, and after/during the previous one
              balanceChangesBeforeDistribution =
                balancesChangesOnThatDay.filter(
                  ({ timestamp: balanceChangeTimestamp }) =>
                    parseInt(balanceChangeTimestamp, 10) <=
                      parseInt(ppsChangeTimestamp, 10) &&
                    parseInt(balanceChangeTimestamp, 10) >
                      parseInt(ppsChangeOnThatDay[_index - 1].timestamp, 10)
                );
            }

            // Update user balance with the latest balance before ratio change
            if (balanceChangesBeforeDistribution.length > 0) {
              currentUserBalance = parseFloat(
                balanceChangesBeforeDistribution[
                  balanceChangesBeforeDistribution.length - 1
                ].value
              );
            }

            // Get underlying asset timestamped price
            const assetPriceAtTimestamp = await getLlamaTokenPriceAtTimestamp(
              underlyingToken,
              parseInt(ppsChangeTimestamp, 10)
            );
            const ratioChange = parseFloat(pricePerShare) - currentRatio;
            assetRewardsOnThatDay += currentUserBalance * ratioChange;
            usdRewardsOnThatDay +=
              currentUserBalance * ratioChange * assetPriceAtTimestamp;
            currentRatio = parseFloat(pricePerShare);
          }
        )
      );
      // Update user balance with the last balance of the day
      currentUserBalance = parseFloat(
        balancesChangesOnThatDay[balancesChangesOnThatDay.length - 1].value
      );
      // Update ratio with the last ratio of the day
      currentRatio = parseFloat(
        ppsChangeOnThatDay[ppsChangeOnThatDay.length - 1].pricePerShare
      );
    }

    cumulativeAsset += assetRewardsOnThatDay;
    cumulativeUSD += usdRewardsOnThatDay;

    userRewardsGraphArray.push({
      timestamp: loopDay.unix().toString(),
      asset: cumulativeAsset,
      usd: cumulativeUSD,
    });
  }

  return userRewardsGraphArray;
};

export const fillInMissingPpsGraphObjects = (
  entitiesArray: StakedSpellPricePerShare[]
): StakedSpellPricePerShare[] => {
  const finalArray: StakedSpellPricePerShare[] = [];
  const startDate = moment(parseInt(entitiesArray[0].timestamp, 10) * 1000);
  const daysUntilToday = moment().diff(startDate, "day");

  for (let i = 0; i <= daysUntilToday; i += 1) {
    const loopDate = moment(startDate).add(i, "day");

    if (loopDate.isSameOrBefore(startDate)) {
      finalArray.push({
        id: `${loopDate.unix()}`,
        timestamp: `${loopDate.unix()}`,
        pricePerShare: "1.0",
        block: "0",
        txHash: "",
      });
    } else {
      const existingEntity = entitiesArray.filter(({ timestamp }) =>
        moment(parseInt(timestamp, 10) * 1000).isSame(loopDate, "day")
      );
      if (existingEntity.length > 0) {
        finalArray.push(existingEntity[existingEntity.length - 1]);
      } else {
        finalArray.push({
          ...finalArray[finalArray.length - 1],
          block: "",
          id: `${loopDate.unix()}`,
          timestamp: `${loopDate.unix()}`,
        });
      }
    }
  }

  return finalArray;
};

export const getLlamaTokenPriceAtTimestamp = async (
  tokenAddress: string,
  timestamp: number,
  network = "ethereum"
): Promise<number> => {
  const tokenChainKey = `${network}:${tokenAddress}`;
  try {
    const response = await axios.get(
      `https://coins.llama.fi/prices/historical/${timestamp}/${tokenChainKey}`
    );
    const tokenPrice = response.data.coins[tokenChainKey].price;

    return tokenPrice;
  } catch (error) {
    console.log(
      `Error fetching historical price of ${tokenChainKey} : ${error}`
    );
    return 0;
  }
};
