export interface StakedSpellPricePerShare {
  id: string;
  block: string;
  timestamp: string;
  txHash: string;
  pricePerShare: string;
}

export interface UserStakedSpellBalance {
  id: string;
  block: string;
  timestamp: string;
  txHash: string;
  event: string;
  value: string;
  user: string;
  transferFrom: string;
  transferTo: string;
}

export interface RewardsDataPoint {
  timestamp: string;
  asset: number;
  usd: number;
}
