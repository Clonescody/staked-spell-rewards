const ZERO_ADDRESS: string = "0x0000000000000000000000000000000000000000";
const SPELL_ADDRESS: string = "0x090185f2135308bad17527004364ebcc2d37e5f6";
const sSPELL_ADDRESS: string = "0x26fa3fffb6efe8c1e69103acb4044c26b9a106a9";

export function handleTransfer(event: TransferEvent): void {
  const amount = event.params.value
    .toBigDecimal()
    .div(BigDecimal.fromString("1e18"));

  const from = event.params.from.toHexString();
  const to = event.params.to.toHexString();
  const idFromTotal = `total:${from}`;
  const idToTotal = `total:${to}`;

  // Any event not listed below is considered a transfer
  let balanceEvent = "transfer";
  // User minted sSPELL => he staked
  if (from == ZERO_ADDRESS) {
    balanceEvent = "stake";
  }
  // User burned his sSPELL => he unstaked
  if (to == ZERO_ADDRESS) {
    balanceEvent = "unstake";
  }

  // ZERO_ADDRESS = staking event, don't register ZERO_ADDRESS's balance
  if (from != ZERO_ADDRESS) {
    let fromTotal = UserBalanceTotal.load(idFromTotal);
    if (fromTotal == null) {
      fromTotal = new UserBalanceTotal(idFromTotal);
      fromTotal.stakedBalance = amount;
    } else {
      fromTotal.stakedBalance = fromTotal.stakedBalance.minus(amount);
    }
    fromTotal.save();

    const fromHistoricalBalance = new UserStakedSpellBalance(
      `${event.block.number}:${from}`
    );
    fromHistoricalBalance.block = event.block.number;
    fromHistoricalBalance.timestamp = event.block.timestamp;
    fromHistoricalBalance.txHash = event.transaction.hash.toHex();
    fromHistoricalBalance.user = from;
    fromHistoricalBalance.value = fromTotal.stakedBalance;
    fromHistoricalBalance.event = balanceEvent;
    fromHistoricalBalance.transferTo = balanceEvent === "transfer" ? to : "";
    fromHistoricalBalance.transferFrom = from;

    fromHistoricalBalance.save();
  }

  // ZERO_ADDRESS = unstaking event, don't register ZERO_ADDRESS's balance
  if (to != ZERO_ADDRESS) {
    let toTotal = UserBalanceTotal.load(idToTotal);
    if (toTotal == null) {
      toTotal = new UserBalanceTotal(idToTotal);
      toTotal.stakedBalance = amount;
    } else {
      toTotal.stakedBalance = toTotal.stakedBalance.plus(amount);
    }
    toTotal.save();

    const toHistoricalBalance = new UserStakedSpellBalance(
      `${event.block.number}:${to}`
    );
    toHistoricalBalance.block = event.block.number;
    toHistoricalBalance.timestamp = event.block.timestamp;
    toHistoricalBalance.txHash = event.transaction.hash.toHex();
    toHistoricalBalance.user = to;
    toHistoricalBalance.value = toTotal.stakedBalance;
    toHistoricalBalance.event = balanceEvent;
    toHistoricalBalance.transferFrom = balanceEvent === "transfer" ? from : "";
    toHistoricalBalance.transferTo = to;

    toHistoricalBalance.save();
  }

  if (balanceEvent !== "transfer") {
    const SpellContract = SPELL.bind(SPELL_ADDRESS);
    const sSpellContract = sSPELL.bind(sSPELL_ADDRESS);
    const balanceOfRaw = SpellContract.balanceOf(sSPELL_ADDRESS);
    const balanceOf = balanceOfRaw
      .toBigDecimal()
      .div(BigDecimal.fromString("1e18"));
    const sSpellSupplyRaw = sSpellContract.totalSupply();
    const sSpellSupply = sSpellSupplyRaw
      .toBigDecimal()
      .div(BigDecimal.fromString("1e18"));
    let stakedSpellpricePerShare = new StakedSpellPricePerShare(
      event.transaction.hash.toHexString()
    );
    stakedSpellpricePerShare.block = event.block.number;
    stakedSpellpricePerShare.timestamp = event.block.timestamp;
    stakedSpellpricePerShare.txHash = event.transaction.hash.toHex();
    stakedSpellpricePerShare.pricePerShare = balanceOf.div(sSpellSupply);
    stakedSpellpricePerShare.save();
  }
}
