const SWAPPER_ADDRESS: string = "0xdfe1a5b757523ca6f7f049ac02151808e6a52111";
const SPELL_ADDRESS: string = "0x090185f2135308bad17527004364ebcc2d37e5f6";
const sSPELL_ADDRESS: string = "0x26fa3fffb6efe8c1e69103acb4044c26b9a106a9";

export function handleTransfer(event: TransferEvent): void {
  if (event.from == SWAPPER_ADDRESS && event.to == sSPELL_ADDRESS) {
    const SpellContract = SPELL.bind(SPELL_ADDRESS);
    const sSpellContract = sSPELL.bind(sSPELL_ADDRESS);
    const balanceOfRaw = SpellContract.balanceOf(sSPELL_ADDRESS);
    const balanceOf = balanceOfRaw
      .toBigDecimal()
      .div(BigDecimal.fromString("1e18"));
    const sSpellSupplyRaw = sSpellContract.totalSupply();
    const sSpellSupply = sSpellSupplyRaw
      .toBigDecimal()
      .div(BigDecimal.fromString("1e18"));
    let stakedSpellpricePerShare = new StakedSpellPricePerShare(
      event.transaction.hash.toHexString()
    );
    stakedSpellpricePerShare.block = event.block.number;
    stakedSpellpricePerShare.timestamp = event.block.timestamp;
    stakedSpellpricePerShare.txHash = event.transaction.hash.toHex();
    stakedSpellpricePerShare.pricePerShare = balanceOf.div(sSpellSupply);
    stakedSpellpricePerShare.save();
  }
}
